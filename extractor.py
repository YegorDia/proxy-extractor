import requests
from BeautifulSoup import BeautifulSoup


def extract_from_sslproxiesorg(session):
    try:
        html = session.get('http://www.sslproxies.org').content
        parsed_html = BeautifulSoup(html)
        proxies_body = parsed_html.body.find('tbody')
        proxies = []
        for child in proxies_body.findChildren():
            settings = child.findAll('td')
            if len(settings) > 0:
                if settings[6].text.find('yes') != -1:
                    proxies.append('http://%s:%s' % (str(settings[0].text), str(settings[1].text)))
        return proxies
    except:
        return []



AVAILABLE_ADDRESES = ['www.sslproxies.org']
EXTRACT_FUNCTIONS = {
    'www.sslproxies.org': extract_from_sslproxiesorg
}


class ProxyExtractor(object):
    def __init__(self):
        self.session = requests.Session()
        self.session.headers.update({'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'})

    def extract_from(self, address='www.sslproxies.org'):
        if address in AVAILABLE_ADDRESES:
            return EXTRACT_FUNCTIONS[address](self.session)


